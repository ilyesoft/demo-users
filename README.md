# Users DEMO
## Spring Boot, Spring MVC, Spring Data, MongoDB, JUnit, Swagger 2.0

This demo uses Spring Boot 2 with Spring Data and Spring MVC to create a swagger-enabled RESTful api for saving and getting users

## requirements:
 - JDK 8
 - Maven
 - Eclipse (Spring Tool Suite version, with [Lombok](https://projectlombok.org/setup/eclipse) extension installed)
 - Optionally MongoDB

## Configuration

MongoDB configuration parameters are set in src\main\resources\application.properties

## Installation

```sh
mvn clean install
java -jar target/demo-users-0.0.1-SNAPSHOT.jar
```

## Swagger API Documentation

Swagger 2.0 api documention is configured and accessible at http://localhost:8080/v2/api-docs/. A swagger UI is also available at http://localhost:8080/swagger-ui/

## Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.4/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.4/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data MongoDB](https://docs.spring.io/spring-boot/docs/2.5.4/reference/htmlsingle/#boot-features-mongodb)

## Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with MongoDB](https://spring.io/guides/gs/accessing-data-mongodb/)
