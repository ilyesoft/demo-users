package com.af.mission.demousers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.List;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.af.mission.demousers.entities.UserEntity;
import com.af.mission.demousers.model.User;
import com.af.mission.demousers.repository.UsersRepository;
import com.af.mission.demousers.web.UsersController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Tests the saving endpoint of user controller
 * @author rhouma
 *
 */
@WebMvcTest(UsersController.class)
class UsersControllerTest {

	private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
	private String path = "/api/users";
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UsersRepository usersRepository;

	/**
	 * Allows testing user saving with all good parameters
	 * @throws Exception
	 */
	@Test
	public void testSaveOkAllFields() throws Exception {

		Mockito.when(usersRepository.insert(Mockito.any(UserEntity.class))).thenAnswer(inv -> {
			
			UserEntity entity = (UserEntity) inv.getArgument(0);
			entity.setId(ObjectId.get());
			return entity;
		});
		User okUser = new User(null, "Someone", "FR", "address", LocalDate.now().minusYears(18), "0102030405", "someone@gmail.com");
	    callPostEndpoint(okUser, status().isCreated());

	}

	/**
	 * Allows testing user saving with bad name parameters
	 * @throws Exception
	 */
	@Test
	public void testSaveErrors() throws Exception {
		User badInputUser = new User(null, "", "a", "address", LocalDate.now().minusYears(18), "55555", "someone@gmail.com");
	    MvcResult result = callPostEndpoint(badInputUser, status().isBadRequest());
	    
	    Exception e = result.getResolvedException();
	    assertTrue(e instanceof MethodArgumentNotValidException, "Expected MethodArgumentNotValidException");

	    List<ObjectError> errors = ((MethodArgumentNotValidException) e).getBindingResult().getAllErrors();
	    
	    assertThat(errors).size().isEqualTo(4);

	}

	/**
	 * Allows testing the birthday validation if the user is one-day younger than 18 years old
	 * @throws Exception
	 */
	@Test
	public void testSaveBirthDate() throws Exception {
		User youngUser = new User(null, "Someone", "FR", "address", LocalDate.now().minusYears(18).plusDays(1), "0102030405", "someone@gmail.com");
	    MvcResult result = callPostEndpoint(youngUser, status().isBadRequest());
	    
	    Exception e = result.getResolvedException();
	    assertTrue(e instanceof MethodArgumentNotValidException, "Expected MethodArgumentNotValidException");

	    List<ObjectError> errors = ((MethodArgumentNotValidException) e).getBindingResult().getAllErrors();
	    
	    assertThat(errors).size().isEqualTo(1);

	}
	
	/**
	 * Allows testing the country validation
	 * @throws Exception
	 */
	@Test
	public void testSaveCountry() throws Exception {
		User youngUser = new User(null, "Someone", "US", "address", LocalDate.now().minusYears(18), "0102030405", "someone@gmail.com");
	    MvcResult result = callPostEndpoint(youngUser, status().isBadRequest());
	    
	    Exception e = result.getResolvedException();
	    assertTrue(e instanceof MethodArgumentNotValidException, "Expected MethodArgumentNotValidException");

	    List<ObjectError> errors = ((MethodArgumentNotValidException) e).getBindingResult().getAllErrors();
	    
	    assertThat(errors).size().isEqualTo(1);

	}
	
	/**
	 * Calls the POST endpoint for user saving, verify the returned HTTP code
	 * 
	 * @param okUser
	 * @param status
	 * @return
	 * @throws JsonProcessingException
	 * @throws Exception
	 */
	private MvcResult callPostEndpoint(User okUser, ResultMatcher status) throws JsonProcessingException, Exception {
		ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
	    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	    String requestJson=mapper.writeValueAsString(okUser);

	    MvcResult result = mockMvc.perform(post(path).contentType(APPLICATION_JSON_UTF8)
	        .content(requestJson)).andExpect(status).andReturn();
	    
	    return result;
	}
}
