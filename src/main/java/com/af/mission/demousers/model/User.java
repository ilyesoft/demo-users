package com.af.mission.demousers.model;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.af.mission.demousers.annotations.IsFrConstraint;
import com.af.mission.demousers.annotations.OnlyAdultConstraint;
import com.af.mission.demousers.entities.UserEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
	
	private String id;
	
	// required name
	@NotEmpty(message = "Name is required")
	private String name;

	// required country, must be equal FR
	@NotEmpty(message = "country is required")
    @Size(min = 2, max = 2, message = "Country Code must be 2 characters")
	@IsFrConstraint
    private String country;
	
	// optional address
	private String address;
	
	// required birth date, must be 18 year old
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonFormat(pattern="yyyy-MM-dd")
	@NotNull
	@OnlyAdultConstraint
	private LocalDate birthDate;
	
	// optional phone number
	@Pattern(regexp="[\\d]{10}")
	private String phoneNumber;
	
	// required email
	@NotEmpty(message = "Name is required")
	@Email(message = "Email should be valid")
	private String email;
	
	public UserEntity toEntity() {
		UserEntity entity = new UserEntity();
		
		entity.setId(null);
		entity.setName(name);
	    entity.setCountry(country);
		entity.setAddress(address);
		entity.setBirthDate(birthDate);
		entity.setPhoneNumber(phoneNumber);
		entity.setEmail(email);

		return entity;
	}
	
	public static User fromEntity(UserEntity entity) {
		User model = new User();
		
		model.setId(entity.getId().toString());
		model.setName(entity.getName());
	    model.setCountry(entity.getCountry());
		model.setAddress(entity.getAddress());
		model.setBirthDate(entity.getBirthDate());
		model.setPhoneNumber(entity.getPhoneNumber());
		model.setEmail(entity.getEmail());

		return model;
	}
}
