package com.af.mission.demousers.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserValidationError implements Serializable{
	private static final long serialVersionUID = 4650511826689778279L;
	
	private String error;
}
