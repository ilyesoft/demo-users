package com.af.mission.demousers.entities;

import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserEntity {
	
	@Id
	private ObjectId id;
	private String name;
    private String country;
	private String address;
	private LocalDate birthDate;
	private String phoneNumber;
	private String email;
}
