package com.af.mission.demousers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Runs the Spring Boot app
 * @author rhouma
 *
 */
@SpringBootApplication
public class DemoUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoUsersApplication.class, args);
	}
}
