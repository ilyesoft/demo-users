package com.af.mission.demousers.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.af.mission.demousers.entities.UserEntity;

public interface UsersRepository extends MongoRepository<UserEntity, String> {

}
