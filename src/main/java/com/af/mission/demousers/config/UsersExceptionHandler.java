package com.af.mission.demousers.config;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.af.mission.demousers.model.UserValidationError;

/**
 * Allows returning custom error message when it comes to User input validation
 * @author rhoum
 *
 */
@ControllerAdvice
public class UsersExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		// retrieve the validation error
		String errorMessage = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
		
		// return HTTP code for bad request with the error message
		return ResponseEntity.badRequest().body(new UserValidationError(errorMessage));
	}
}