package com.af.mission.demousers.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import com.af.mission.demousers.annotations.TimeLogged;

import lombok.extern.slf4j.Slf4j;

/**
 * A Spring Aspect for logging the input and output of a method call and the time spent
 * using the annotation {@link TimeLogged}
 * @author rhouma
 *
 */
@Aspect
@Slf4j
@Component
@ConditionalOnExpression("${aspect.enabled:true}")
public class ExecutionTimeLogAspect {
	
    @Around("@annotation(com.af.mission.demousers.annotations.TimeLogged)")
    public Object executionTime(ProceedingJoinPoint point) throws Throwable {
    	// save the call start time
        long startTime = System.currentTimeMillis();
        // log the input
        log.info("Controller: "+ point.getSignature().getDeclaringTypeName() +". Endpoint: "+ point.getSignature().getName() + ". Args : " + point.getArgs());
        
        // make the actual call
        Object object = point.proceed();
        
        // save the call completed time
        long endtime = System.currentTimeMillis();
        // log the output
        log.info("Controller: "+ point.getSignature().getDeclaringTypeName() +". Endpoint: "+ point.getSignature().getName() + ". Output : " + object);
        
        // log the time spend in the call
        log.info("Controller: "+ point.getSignature().getDeclaringTypeName() +". Endpoint: "+ point.getSignature().getName() + ". Time taken for Execution is : " + (endtime-startTime) +"ms");
        
        // return the output
        return object;
    }

}
