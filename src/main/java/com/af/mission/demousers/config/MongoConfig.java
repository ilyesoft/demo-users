package com.af.mission.demousers.config;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.ImmutableMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;

@Configuration
@EnableConfigurationProperties(AFMongoProperties.class)
@EnableMongoRepositories(basePackages = "com.af.mission.demousers.repository")
public class MongoConfig {

	@Autowired private AFMongoProperties mongoProperties;

	/**
	 * A destroyable bean for the embedded MongoDb server
	 * 
	 * @return
	 * @throws IOException
	 */
	@Bean(destroyMethod="stop")
	public MongodExecutable mongodExecutable() throws IOException {
        ImmutableMongodConfig mongodConfig = MongodConfig
                .builder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(mongoProperties.getHost(),  mongoProperties.getPort(), false))
                .build();

        MongodStarter starter = MongodStarter.getDefaultInstance();
        MongodExecutable mongodExecutable = starter.prepare(mongodConfig);

        // if embedded is tru in the properties, start the embedded MongoDb server
        if (mongoProperties.isEmbedded()) {
	        mongodExecutable.start();
		}
		
		// return the executable mongodb as a bean to be shutdown automatically
		return mongodExecutable;
	}
	
	/**
	 * MongoDb client bean
	 * @param mongodExecutable
	 * @return
	 * @throws IOException
	 */
	@Autowired
	@Bean
	public MongoClient mongoClient(MongodExecutable mongodExecutable) throws IOException {

		return MongoClients.create("mongodb://" + mongoProperties.getHost() + ":" + mongoProperties.getPort());
	}

	/**
	 * MongoDb template bean
	 * @param mongoClient
	 * @return
	 * @throws Exception
	 */
	@Autowired
	@Bean
	public MongoTemplate mongoTemplate(MongoClient mongoClient) throws Exception {
		return new MongoTemplate(mongoClient, "AFUsersDemo");
	}
}
