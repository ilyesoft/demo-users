package com.af.mission.demousers.config;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.af.mission.demousers.annotations.IsFrConstraint;

/**
 * Configures a validator using the annotation {@link IsFrConstraint}
 * It validated the user input by making sure that it's country address is FR
 * @author rhouma
 *
 */
public class IsFrValidator implements ConstraintValidator<IsFrConstraint, String> {

	private static final String COUNTRY_CODE_FRANCE = "FR";

	@Override
	public void initialize(IsFrConstraint country) {
	}

	@Override
	public boolean isValid(String country, ConstraintValidatorContext cxt) {
		return COUNTRY_CODE_FRANCE.equals(country);
	}
}