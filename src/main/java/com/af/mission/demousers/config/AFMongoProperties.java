package com.af.mission.demousers.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix="af.users.mongo")
@Getter
@Setter
public class AFMongoProperties {

	/**
	 * choose if to run an embedded version of MongoDb
	 */
	private boolean embedded;
	
	/**
	 * MongoDb host: use localhost as default
	 */
	private String host;
	
	/**
	 * MongoDb port: use 27017 as default
	 */
	private int port;
}
