package com.af.mission.demousers.config;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.af.mission.demousers.annotations.OnlyAdultConstraint;

/**
 * Configures a validator using the annottaion {@link OnlyAdultConstraint}
 * It validated the user input by making sure that it's age is more than 18 years
 * @author rhoum
 *
 */
public class OnlyAdultValidator implements ConstraintValidator<OnlyAdultConstraint, LocalDate> {

	private static final int ADULT_MIMINUM_AGE = 18;

	@Override
	public void initialize(OnlyAdultConstraint birthDate) {
	}

	@Override
	public boolean isValid(LocalDate birthDateField, ConstraintValidatorContext cxt) {
		// checks that the difference between birth day and today is more or equal than than 18 years 
		return birthDateField != null && birthDateField.until(LocalDate.now()).getYears() >= ADULT_MIMINUM_AGE;
	}
}