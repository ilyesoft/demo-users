package com.af.mission.demousers.web;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.af.mission.demousers.annotations.TimeLogged;
import com.af.mission.demousers.entities.UserEntity;
import com.af.mission.demousers.model.User;
import com.af.mission.demousers.repository.UsersRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Parameter;

/**
 * Users controller, allows accessing the users REST resources for retrieving and saving
 * 
 * @author rhouma
 *
 */
@RestController
@RequestMapping("api/users")
public class UsersController {
	
	@Autowired
	private UsersRepository repository;

	/**
	 * Access to all users resources
	 * @return all users
	 */
	@ApiOperation(value = "Use to get all users")
	@TimeLogged
	@GetMapping(produces = "application/json")
	public List<User> getAll() {
		return repository.findAll().stream().map(entity -> User.fromEntity(entity)).collect(Collectors.toList());
	}

	/**
	 * Access to one user's resource by its ID
	 * 
	 * @param id
	 * @return the found users resource
	 */
	@ApiOperation(value = "Use to find a specific user by his id")
	@TimeLogged
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Object> getById(@PathVariable String id) {
		Optional<UserEntity> foundUser = repository.findById(id);
		if (foundUser.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(User.fromEntity(foundUser.get()));
	}

	/**
	 * Access to users resource for saving using the POST method
	 * 
	 * @param user
	 * @param onlyValidate Only validate input and do not save it, by default false
	 */
	@ApiOperation(value = "Use to save a new user")
	@TimeLogged
	@PostMapping(consumes = "application/json")
	public ResponseEntity<Void> save(
			@RequestBody @Valid User user,
			@Parameter(description = "true to only validate user data without saving") @RequestParam(defaultValue = "false") boolean onlyValidate) {
		if (!onlyValidate) {
			UserEntity createdUser = repository.insert(user.toEntity());
			return ResponseEntity.created(URI.create("/" + createdUser.getId().toString())).build();
		} else {
			return ResponseEntity.ok().build();
		}
	}
}
