package com.af.mission.demousers.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.af.mission.demousers.validation.OnlyAdultValidator;

@Documented
@Constraint(validatedBy = OnlyAdultValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface OnlyAdultConstraint {
    String message() default "Age must be 18 or more";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}