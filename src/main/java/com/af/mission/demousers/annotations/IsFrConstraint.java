package com.af.mission.demousers.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.af.mission.demousers.validation.IsFrValidator;

@Documented
@Constraint(validatedBy = IsFrValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IsFrConstraint {
    String message() default "Country must be FR";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}