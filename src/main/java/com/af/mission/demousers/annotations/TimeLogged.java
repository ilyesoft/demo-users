package com.af.mission.demousers.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.af.mission.demousers.config.ExecutionTimeLogAspect;

/**
 * Annotation for logging the input and output of a method call and the time spent
 * using the Aspect at {@link ExecutionTimeLogAspect}
 * @author rhouma
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TimeLogged {

}
